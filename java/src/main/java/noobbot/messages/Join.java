/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Join extends SendMsg {
    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}