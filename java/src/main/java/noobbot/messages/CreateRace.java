/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

import noobbot.messages.createrace.BotID;
import noobbot.messages.createrace.CreateRaceData;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class CreateRace extends Join {
    //maps
    public static final String MAP_KEIMOLA = "keimola";
    public static final String MAP_USA = "usa";
    public static final String MAP_GERMANY = "germany";
    public static final String MAP_FRANCE = "france";
    
    //other settings
    public static final String PASSWORD = "lalala";
    public static final int CARCOUNT = 1;
    
    //data to send
    private final CreateRaceData data;
    
    public CreateRace(final String name, final String key, String map) {
        super(name, key);
        
        data = new CreateRaceData(new BotID(name, key), map, PASSWORD, CARCOUNT);
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
    
    @Override
    protected Object msgData() {
        return data;
    }
}