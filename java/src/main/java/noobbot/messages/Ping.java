/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}