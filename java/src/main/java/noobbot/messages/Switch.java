/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Switch extends SendMsg {
    public static final String SWITCH_TO_LEFT = "Left";
    public static final String SWITCH_TO_RIGHT = "Right";
    
    private final String mSwitchTo;

    public Switch(String switchTo) {
        this.mSwitchTo = switchTo;
    }
    
    @Override
    protected Object msgData() {
        return mSwitchTo;
    }
    
    @Override
    protected String msgType() {
        return "switchLane";
    }
}