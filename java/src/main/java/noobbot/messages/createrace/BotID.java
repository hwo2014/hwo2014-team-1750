/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.createrace;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class BotID {
    private String name;
    private String key;
    
    public BotID(String name, String key) {
        this.name = name;
        this.key = key;
    }
}
