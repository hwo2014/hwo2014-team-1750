/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.createrace;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class CreateRaceData {
    private BotID botId;
    private String trackName;
    private String password;
    private int carCount;
    
    public CreateRaceData(BotID botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.carCount = carCount;
        this.password = password;
        this.trackName = trackName;
    }
}
