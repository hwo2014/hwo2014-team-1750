/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class TurboBoost extends SendMsg {
    private final static String BOOST_MSG = "pew pew pew";
    
    @Override
    protected Object msgData() {
        return BOOST_MSG;
    }
    
    @Override
    protected String msgType() {
        return "turbo";
    }
}