/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class MsgWrapper {
    public final String msgType;
    public final Object data;

    public MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}