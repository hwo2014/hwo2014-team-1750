package noobbot;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import noobbot.Json.GameInfo;
import noobbot.Json.Piece;
import noobbot.Json.carpositions.CarPositionWrapper;
import noobbot.Json.turbo.TurboWrapper;
import noobbot.messages.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        System.out.println("OK");
        new Main(reader, writer, new Join(botName, botKey));
    }
    
    private ArrayList<Piece> mPieces;
    private GameInfo mGameInfo;
    final Gson mGson = new Gson();
    private PrintWriter mWriter;
    private int carIndex = 0;
    boolean temp = true;

    /*
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        try (BufferedWriter write = new BufferedWriter(new FileWriter("race-info-" + System.currentTimeMillis() + ".txt"))) {

            send(join);

            while((line = reader.readLine()) != null) {
                final MsgWrapper msgFromServer = mGson.fromJson(line, MsgWrapper.class);
                
                write.write(line);
                write.newLine();
                
                if (msgFromServer.msgType.equals("carPositions")) {

                    send(new Throttle(0.6));
                    
                } else if (msgFromServer.msgType.equals("join")) {

                    System.out.println("Joined");

                } else if (msgFromServer.msgType.equals("gameInit")) {
                    
                    parseGameInfo(line);
                    
                    System.out.println("Race init");

                } else if (msgFromServer.msgType.equals("gameEnd")) {

                    System.out.println("Race end");

                } else if (msgFromServer.msgType.equals("gameStart")) {

                    System.out.println("Race start");

                } else {
                    send(new Ping());
                }
            }
        }
    }
    //*/
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.mWriter = writer;
        String line;
        CarPositionWrapper lastCp = null;
        double velocity = 0;
        
        File logs = new File("logs");
        if (!logs.exists())
            logs.mkdir();
        
        try (BufferedWriter write = new BufferedWriter(new FileWriter("logs/race-info-" + System.currentTimeMillis() + ".txt"))) {
            
            send(join);
            //send(new CreateRace(join.key, join.key, CreateRace.MAP_FRANCE));
            while((line = reader.readLine()) != null) {
                
                write.write(line);
                write.newLine();
                
                final MsgWrapper msgFromServer = mGson.fromJson(line, MsgWrapper.class);
                
                if (msgFromServer.msgType.equals("carPositions")) {
                    final CarPositionWrapper cp = mGson.fromJson(line, CarPositionWrapper.class);
                    
                    send(computeAction(lastCp, cp));
                    
                    if (velocity <= calculateVelocity(lastCp, cp) && false) {
                        velocity = calculateVelocity(lastCp, cp);/*
                        System.out.print("lap: " + cp.getData().get(carIndex).getLap());
                        System.out.print(", track piece: " + cp.getData().get(carIndex).getPiecePosition().getPieceIndex());
                        System.out.println(", distance: " + cp.getData().get(carIndex).getPiecePosition().getInPieceDistance());
                        */
                    }
                    else {
                        System.out.println("Max velocity achieved: " + velocity);
                        System.out.print("Track piece: " + cp.getData().get(carIndex).getPiecePosition().getPieceIndex());
                        System.out.println(", distance: " + cp.getData().get(carIndex).getPiecePosition().getInPieceDistance());
                        System.out.println("Piece angle: " + mPieces.get(cp.getData().get(carIndex).getPiecePosition().getPieceIndex()).getAngle());
                        System.out.println("Piece radius: " + mPieces.get(cp.getData().get(carIndex).getPiecePosition().getPieceIndex()).getRadius());
                        System.out.println("Car last angle: " + cp.getData().get(carIndex).getAngle());
                        velocity = 0;
                        //break;
                    }
                    lastCp = cp;

                } else if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    line = line.replaceAll("switch", "Switch"); //because switch is a "Java-word" we can't parse it
                    parseGameInfo(line);
                    //gia na mhn exw polles klhseis, mporoume na to bgaloume an den saresei
                    mPieces = mGameInfo.getData().getRace().getTrack().getPieces();
                    
                    //Get the correct car index instead of guessing it's 0
                    for (int i = 0; i < mGameInfo.getData().getRace().getCars().size(); i++) {
                        if (mGameInfo.getData().getRace().getCars().get(i).getId().getName().equals(join.name)) {
                            carIndex = i;
                            break;
                        }
                    }
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    System.out.println("Race end");
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    System.out.println("Race start");
                } else if (msgFromServer.msgType.equals("turboAvailable")) {
                    System.out.println("Turbo available");
                    //Get the turbo info
                    TurboWrapper tw = mGson.fromJson(line, TurboWrapper.class);
                    System.out.println("");
                    System.out.println(tw.getGameId());
                    System.out.println(tw.getGameTick());
                    System.out.println(tw.getMsgType());
                    System.out.println(tw.getData().getTurboDurationMilliseconds());
                    System.out.println(tw.getData().getTurboDurationTicks());
                    System.out.println(tw.getData().getTurboFactor());
                    //break;
                } else {
                    send(new Ping());
                }
            }
        }
    }
    
    private void send(final SendMsg msg) {
        mWriter.println(msg.toJson());
        mWriter.flush();
    }
    
    private void parseGameInfo(String line) {
        mGameInfo = mGson.fromJson(line, GameInfo.class);
    }
    
    /**
     * Calculates car's velocity based on its last position.
     * 
     * @param lastCp The car's last position
     * @param cp The car's current position
     * @return The car's velocity in distance/tick
     */
    private double calculateVelocity (CarPositionWrapper lastCp, CarPositionWrapper cp) {
        double dx; //Distance delta
        double dt; //Tick delta
        
        //First position of the race
        if (lastCp == null)
            return 0;
        
        //Both positions on the same piece
        if (lastCp.getData().get(carIndex).getPiecePosition().getPieceIndex() == cp.getData().get(carIndex).getPiecePosition().getPieceIndex()) {
            dx = cp.getData().get(carIndex).getPiecePosition().getInPieceDistance() - lastCp.getData().get(carIndex).getPiecePosition().getInPieceDistance();
            dt = cp.getGameTick() - lastCp.getGameTick();
            
        }
        else { //Positions on different pieces
            //Calculate remaining distance on last position's piece
            dx = mPieces.get(lastCp.getData().get(carIndex).getPiecePosition().getPieceIndex()).getLength() - lastCp.getData().get(carIndex).getPiecePosition().getInPieceDistance();
            //Add the distance traveled in the next piece
            dx += cp.getData().get(carIndex).getPiecePosition().getInPieceDistance();
            dt = cp.getGameTick() - lastCp.getGameTick();
            
        }
        
        if (dt != 0) {
            return dx / dt;
        }
        else {
            return 0;
        }
    }
    
    /**
     * The heart of the car's AI logic.
     * Computes the next action to be taken and returns an instance of a SendMsg class's child.
     * 
     * @param cp The car's current position
     * @return Child object of SendMsg class
     * @see SendMsg
     */
    private SendMsg computeAction(CarPositionWrapper lastCp, CarPositionWrapper cp) { //Add last position as a parameter or keep a list of the cars previous positions, to comply with Idea 1
        //TODO: AI logic
        //Idea 1: compute car velocity (probably will need to keep track of the car's last position)
        //Idea 2: compute car velocity decay over time when throttle is 0
        //Idea 3: compute maximum safe velocity in acordance to turn angle-radius
        //Lastly return the highest "safe" throttle, or a necessary switch maneuver

        double velocity = calculateVelocity(lastCp, cp);
        double thr;
        //System.out.println(velocity);
        //return new Throttle(1.0);
        /*
        if (velocity < 6) {
            return new Throttle(1.0);
        }
        else if (velocity < 6.5) {
            return new Throttle (0.7);
        }
        else if (velocity < 7) {
            return new Throttle(0.6);
        }
        else if (velocity < 8) {
            return new Throttle(0.5);
        }
        else {
            return new Throttle(0);
        }/*/
        //An other approach for the AI, looking the curve in the following track
        //and use that a sign for the throttle
        //TODO: try to use differentiation of the angle
        //TODO: use both of the methods, hybrid
        double curve = getCurveOn(cp, 300);
        if (curve < 50) {
            thr = 1;
        } else {
            thr = 0.6;
        }
        return new Throttle(thr);//*/
    }
    
    /**
     * Computes the abs val of the curve in the following distanceToSearch distance
     * @param cp The Car's current position
     * @param distanceToSearch The distance in we want to check the curve
     * @return the curve in the following distance
     */
    private double getCurveOn(CarPositionWrapper cp, double distanceToSearch) {
        int currentPiecePos = cp.getData().get(carIndex).getPiecePosition().getPieceIndex();
        double curve = 0;
        double currentDistance = 0;
        double inPieceDist = cp.getData().get(carIndex).getPiecePosition().getInPieceDistance();
        
        while (currentDistance < distanceToSearch) {
            if (mPieces.get(currentPiecePos).getLength() != 0) {
                currentDistance += mPieces.get(currentPiecePos).getLength() - inPieceDist;
            } else {
                double tempDist;
                //angle in radians\\ * r = length
                tempDist = Math.toRadians(mPieces.get(currentPiecePos).getAngle()) * mPieces.get(currentPiecePos).getRadius() - inPieceDist;
                currentDistance += tempDist;
                //r = len / angle
                if (currentDistance > distanceToSearch) {
                    //Remove the extending piece so we have an aqurate result
                    double toSubtract = currentPiecePos - distanceToSearch;
                    curve += Math.toDegrees(Math.abs(tempDist - toSubtract / Math.toRadians(mPieces.get(currentPiecePos).getAngle())));
                } else {
                    curve += Math.abs(tempDist / Math.toRadians(mPieces.get(currentPiecePos).getAngle()));
                }
            }
            inPieceDist = 0;
            currentPiecePos = (++currentPiecePos) % mPieces.size();
        }
        
        return curve;
    }
}