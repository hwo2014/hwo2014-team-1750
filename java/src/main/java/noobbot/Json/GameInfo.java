/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Chris Tryf <chritryf@csd.auth.gr>
 */
public class GameInfo {
    private String msgType;
    private Data data;
    private String gameId;
            
    public String getMsgType() {
        return msgType;
    }
    
    public Data getData() {
        return data;
    }
    
    public String getGameId () {
        return gameId;
    }
}
