/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Car {
    private Id id;
    private Dimensions dimensions;
    
    public Id getId () {
        return id;
    }
    
    public Dimensions getDimensions () {
        return dimensions;
    }
    
    @Override
    public String toString() {
        return id.toString() + ", " + dimensions.toString();
    }
}
