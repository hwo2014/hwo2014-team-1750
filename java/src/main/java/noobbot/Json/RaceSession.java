/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class RaceSession {
    private int laps;
    private int maxLapTimeMs;
    private boolean quickRace;
    
    public int getLaps () {
        return laps;
    }
    
    public int getMaxLapTimesMs () {
        return maxLapTimeMs;
    }
    
    public boolean getQuickRace () {
        return quickRace;
    }
}
