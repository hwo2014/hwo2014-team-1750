/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

import java.util.ArrayList;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Track {
    private String id;
    private String name;
    private ArrayList<Piece> pieces;
    private ArrayList<Lane> lanes;
    private StartingPoint startingPoint;
    
    public String getId () {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public ArrayList<Piece> getPieces () {
        return pieces;
    }
    
    public ArrayList<Lane> getLanes () {
        return lanes;
    }
    
    public StartingPoint getStartingPoint () {
        return startingPoint;
    }
}
