/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Position {
    private double x;
    private double y;
    
    public double getX () {
        return x;
    }
    
    public double getY () {
        return y;
    }
}
