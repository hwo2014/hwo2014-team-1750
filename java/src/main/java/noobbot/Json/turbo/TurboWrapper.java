/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.turbo;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class TurboWrapper {
    private String msgType;
    private TurboInfo data;
    private String gameId;
    private int gameTick;
    
    public String getMsgType() {
        return msgType;
    }
    
    public TurboInfo getData() {
        return data;
    }
    
    public String getGameId() {
        return gameId;
    }
    
    public int getGameTick() {
        return gameTick;
    }
}
