/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.turbo;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class TurboInfo {
    private double turboDurationMilliseconds;
    private int turboDurationTicks;
    private double turboFactor;
    
    public double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }
    
    public int getTurboDurationTicks() {
        return turboDurationTicks;
    }
    
    public double getTurboFactor() {
        return turboFactor;
    }
}
