/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Dimensions {
    private double length;
    private double width;
    private double guideFlagPosition;
    
    public double getCarLength () {
        return length;
    }
    
    public double getCarWidth () {
        return width;
    }
    
    public double getCarGuideFlagPosition () {
        return guideFlagPosition;
    }
    
    @Override
    public String toString() {
        return "l: " + length + ", w: " + width + ", g: " + guideFlagPosition;
    }
}
