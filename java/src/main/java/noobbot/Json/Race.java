/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

import java.util.ArrayList;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Race {
    private Track track;
    private ArrayList<Car> cars;
    private RaceSession raceSession;
    
    public Track getTrack () {
        return track;
    }
    
    public ArrayList<Car> getCars () {
        return cars;
    }
    
    public RaceSession getRaceSession () {
        return raceSession;
    }
}