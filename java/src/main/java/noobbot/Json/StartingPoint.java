/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class StartingPoint {
    private Position position;
    private double angle;
    
    public Position getPosition () {
        return position;
    }
    
    public double getAngle() {
        return angle;
    }
}
