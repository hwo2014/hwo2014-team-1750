/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.carpositions;

import noobbot.Json.Id;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class CarPosition {
    private Id id;
    private double angle;
    private PiecePosition piecePosition;
    private int lap;
    
    public Id getId () {
        return id;
    }
    
    public double getAngle () {
        return angle;
    }
    
    public PiecePosition getPiecePosition () {
        return piecePosition;
    }
    
    public int getLap () {
        return lap;
    }
    
    @Override
    public String toString () {
        return null;
    }
}
