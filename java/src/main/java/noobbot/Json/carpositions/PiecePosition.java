/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.carpositions;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class PiecePosition {
    private int pieceIndex;
    private double inPieceDistance;
    private LaneCarP lane;
    
    public int getPieceIndex () {
        return pieceIndex;
    }
    
    public double getInPieceDistance () {
        return inPieceDistance;
    }
    
    public LaneCarP getLane () {
        return lane;
    }
    
}
