/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.carpositions;

import java.util.ArrayList;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class CarPositionWrapper {
    private String msgType;
    private ArrayList<CarPosition> data;
    private String gameId;
    private int gameTick;
    
    public ArrayList<CarPosition> getData () {
        return data;
    }
    
    public String getGameId () {
        return gameId;
    }
    
    public int getGameTick () {
        return gameTick;
    }
    
    public String getMsgType () {
        return msgType;
    }
}
