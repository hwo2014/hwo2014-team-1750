/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json.carpositions;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class LaneCarP {
    private int startLaneIndex;
    private int endLaneIndex;
    
    public int getStartLaneIndex () {
        return startLaneIndex;
    }
    
    public int getEndLaneIndex () {
        return endLaneIndex;
    }

}
