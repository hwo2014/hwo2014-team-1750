/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Id {
    private String name;
    private String color;
    
    public String getName () {
        return name;
    }
    
    public String getColor () {
        return color;
    }
    
    @Override
    public String toString () {
        return name + ", " + color;
    }
}
