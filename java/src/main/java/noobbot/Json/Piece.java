/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.Json;

/**
 *
 * @author Philip Lanaras <flanaras@csd.auth.gr>
 */
public class Piece {
    private double length;
    private boolean Switch;
    private int radius;
    private double angle;
    
    public double getLength () {
        return length;
    }
    
    public boolean getSwitch () {
        return Switch;
    }
    
    public int getRadius () {
        return radius;
    }
    
    public double getAngle () {
        return angle;
    }
    
    @Override
    public String toString () {
        return "[len:" + length + ", sw: " + Switch + ", radius: " + radius + ", angle: " + angle + "]";
    }
}
